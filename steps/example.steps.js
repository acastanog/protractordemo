// prueba
let {defineSupportCode} = require('cucumber');
const { browser, WebDriver } = require('protractor');
const { WebdriverBy } = require('protractor/built/locators');
let chai = require('chai').use(require('chai-as-promised'));
let expect = chai.expect;
defineSupportCode( function({When, Then}) {
 When('Ingreso a {string}', function(site) {
   return browser.get(site);
 });
 
 Then('comparo el titulo {string}', function(title) {
   return expect(browser.getTitle()).to.eventually.eql(title);
 });
 
 When('Ingreso a la seccion de Docs', function() {
   var docButton = element(by.xpath('//*[@id="intro"]/div[2]/a'));
   return docButton.click();
 });
 
 Then('puedo ver  el articulo de angular', function() {
   var article = element(by.xpath('//*[@id="docs"]/aio-doc-viewer/div/div[2]/div/a[1]/section'));
   return article.click();
 });

 Then('veo la guia de instalacion de angular', function() {
  var angularMenu = element(by.xpath('/html/body/aio-shell/mat-sidenav-container/' +
  'mat-sidenav/div/aio-nav-menu/aio-nav-item[2]/div/div/aio-nav-item[2]/div/button'));
  var angular = element(by.xpath('/html/body/aio-shell/mat-sidenav-container/mat-sidenav/div/' +
  'aio-nav-menu/aio-nav-item[2]/div/div/aio-nav-item[2]/div/div/aio-nav-item[1]/div/a'));
  return angularMenu.click(),angular.click();
});
});